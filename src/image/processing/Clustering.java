package image.processing;

import image.processing.algorithms.clustering.kmeans.AlgoHierarchicalClustering;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import image.processing.algorithms.clustering.kmeans.AlgoKMeansCol;
import image.processing.algorithms.clustering.kmeans.AlgoKMeansFeat;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Clustering {
	
    public static double sumC ;
    public static double sumF;
    
        //Function to compile a text file of color data
        //That the algorithm can process
        public static void makeColDataFile() throws FileNotFoundException, UnsupportedEncodingException, IOException
        {
            try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(".//src//image//processing//current_colour_data.txt", false)))) {
                for(int i = 0; i < ImageSorter.photoSet.length; i++)
                {
                    for (int j = 0; j < ImageSorter.photoSet[i].colourBin.length; j++)
                    {
                        writer.write(ImageSorter.photoSet[i].colourBin[j] + " ");
                    }
                    writer.println("\n");
                }
            }
        }
        
        //Function to compile a text file of feature data
        //That the algorithm can process
        public static void makeFeatDataFile() throws FileNotFoundException, UnsupportedEncodingException, IOException
        {
            try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(".//src//image//processing//current_feature_data.txt", false)))) {
                for(int i = 0; i < ImageSorter.photoSet.length; i++)
                {
                    for (int j = 0; j < ImageSorter.photoSet[i].featureBin.length; j++)
                    {
                        writer.write(ImageSorter.photoSet[i].featureBin[j] + " ");
                    }
                    writer.println("\n");
                }
            }
        }
        
        //Function to compile a text file of feature data
        //That the algorithm can process
        public static void makeCombDataFile() throws FileNotFoundException, UnsupportedEncodingException, IOException
        {
            try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(".//src//image//processing//current_combined_data.txt", false)))) {
                
                for(int i = 0; i < ImageSorter.photoSet.length; i++)
                {
                    
                    sumC = 0;
                    sumF = 0;
                    
                    //calculate sum of colour bins and print to file
                    for (int j = 0; j < ImageSorter.photoSet[i].colourBin.length; j++)
                    {
                        sumC += ImageSorter.photoSet[0].colourBin[j];
                    }
                    for (int j = 0; j < ImageSorter.photoSet[i].colourBin.length; j++)
                    {
                        writer.write(round(((ImageSorter.photoSet[i].colourBin[j])/sumC), 2) + " ");
                    }
                    
                    //calculate sum of feature bins and print to file
                    for (int j = 0; j < ImageSorter.photoSet[i].featureBin.length; j++)
                    {
                        sumF += ImageSorter.photoSet[0].featureBin[j];
                    }
                    for (int j = 0; j < ImageSorter.photoSet[i].featureBin.length; j++)
                    {
                        writer.write(round(((ImageSorter.photoSet[i].featureBin[j])/sumF), 2) + " ");
                    }
                    
                    writer.println("\n");
                }
            }
        }
        
        //Call the hierarchical clustering algorithm on the color data
        //Argument is the max distance for merging clusters
        //Modified from interface slider
        public static void hierarchicalColor(int _k) throws NumberFormatException, IOException{
		
		String input = fileToPath("current_colour_data.txt");
		String output = ".//output_colour.txt";
		int maxdistance = _k;
		
		// Apply the algorithm
		AlgoHierarchicalClustering algo = new AlgoHierarchicalClustering();  // we request 3 clusters
		algo.runAlgorithm(input, maxdistance);
		algo.printStatistics();
		algo.saveToColFile(output);

	}
        
        //Call the hierarchical clustering algorithm on the feature data
        //Argument is the max distance for merging clusters
        //Modified from interface slider
        public static void hierarchicalFeature(int _k) throws NumberFormatException, IOException{
		
		String input = fileToPath("current_feature_data.txt");
		String output = ".//output_feature.txt";
		int maxdistance = _k;
		
		// Apply the algorithm
		AlgoHierarchicalClustering algo = new AlgoHierarchicalClustering();  // we request 3 clusters
		algo.runAlgorithm(input, maxdistance);
		algo.printStatistics();
		algo.saveToFeatFile(output);

	}
        
        //Call the hierarchical clustering algorithm on the combined data
        //Argument is the max distance for merging clusters
        //Modified from interface slider
        public static void hierarchicalCombined(double _k) throws NumberFormatException, IOException{
		
		String input = fileToPath("current_combined_data.txt");
		String output = ".//output_combined.txt";
		double maxdistance = _k;
		
		// Apply the algorithm
		AlgoHierarchicalClustering algo = new AlgoHierarchicalClustering();  // we request 3 clusters
		algo.runAlgorithm(input, maxdistance);
		algo.printStatistics();
		algo.saveToCombFile(output);

	}
    
	public static void kMeansColour(int _k) throws NumberFormatException, IOException{
            
                //disable button
                Interface.clusterButton.setEnabled(false);
            
		String input = fileToPath("current_colour_data.txt");
		String output = ".//output_colour.txt";
		int k = _k;
                
		// Apply the algorithm for colour
		AlgoKMeansCol algoKMeans = new AlgoKMeansCol();
		algoKMeans.runAlgorithm(input, k);
		algoKMeans.printStatistics();
		algoKMeans.saveToFile(output);
                
                //Reenable button
                Interface.clusterButton.setEnabled(true);

	}
        
        public static void kMeansFeature(int _k) throws NumberFormatException, IOException{
            
                //disable button
                Interface.clusterButton.setEnabled(false);
                
		String input = fileToPath("current_feature_data.txt");
		String output = ".//output_feature.txt";
		int k = _k;
                
		// Apply the algorithm for colour
		AlgoKMeansFeat algoKMeans = new AlgoKMeansFeat();
		algoKMeans.runAlgorithm(input, 3);
		algoKMeans.printStatistics();
		algoKMeans.saveToFile(output);
                
                //Reenable button
                Interface.clusterButton.setEnabled(true);

	}
	
	public static String fileToPath(String filename) throws UnsupportedEncodingException{
                return "C:\\Users\\Catbug\\Documents\\ImageProcessing\\src\\image\\processing\\" + filename;
		//URL url = Clustering.class.getResource(filename);
		 //return java.net.URLDecoder.decode(url.getPath(),"UTF-8");
	}
        
        public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
	
	
}
