
package image.processing;

public class Pair {
    
    public int num;
    public int value;
    
    public Pair(int _num, int _value) {
        this.num = _num;
        this.value = _value;
    }
    
    public int getNum()
    {
        return this.num;
    }
    
    public int getValue()
    {
        return this.value;
    }
    
    public void giveNum(int _num)
    {
        this.num = _num;
    }
    
    public void giveValue(int _value)
    {
        this.value= _value;
    }
}
