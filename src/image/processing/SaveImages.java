
package image.processing;

import javax.swing.JOptionPane;

public class SaveImages {
    
    private static int i,j;
    private static int[] tempArray;
    private static String results;
    
    
    public static void printColourHierarchy()
    {
        results = ("<html><body width = '300'><h1> Colour Results: </h1>");
        for(i = 0; i < ImageSorter.colClusterOrder.size(); i++)
        {
            if(ImageSorter.colClusterOrder.get(i).length >= 1){
                results += ("<br>Folder " + i + ":<br>");
                for(j = 0; j < ImageSorter.colClusterOrder.get(i).length; j++)
                {
                    
                    tempArray = ImageSorter.colClusterOrder.get(i);
                    results += ("-contains: " + ImageSorter.photoSet[tempArray[j]].getShortName() + "<br>");
                    
                }
            }
            else{
                results += ("<br>Folder " + i + ": EMPTY <br>");
            }
        }
        JOptionPane.showMessageDialog(Interface.mainContainer, results);
    }//printImageHierarchy
    
    
    public static void printFeatureHierarchy()
    {
        results = ("<html><body width = '300'><h1> Feature Results: </h1>");
        for(i = 0; i < ImageSorter.featClusterOrder.size(); i++)
        {
            if(ImageSorter.featClusterOrder.get(i).length >= 1){
                results += ("<br>Folder " + i + ":<br>");
                for(j = 0; j < ImageSorter.featClusterOrder.get(i).length; j++)
                {
                    
                    tempArray = ImageSorter.featClusterOrder.get(i);
                    results += ("-contains: " + ImageSorter.photoSet[tempArray[j]].getShortName() + "<br>");
                    
                }
            }
            else{
                results += ("<br>Folder " + i + ": EMPTY <br>");
            }
        }
        JOptionPane.showMessageDialog(Interface.mainContainer, results);
    }//printImageHierarchy
    
    public static void printCombinedHierarchy()
    {
        results = ("<html><body width = '300'><h1> Combined Results: </h1>");
        for(i = 0; i < ImageSorter.combClusterOrder.size(); i++)
        {
            if(ImageSorter.combClusterOrder.get(i).length >= 1){
                results += ("<br>Folder " + i + ":<br>");
                for(j = 0; j < ImageSorter.combClusterOrder.get(i).length; j++)
                {
                    
                    tempArray = ImageSorter.combClusterOrder.get(i);
                    results += ("-contains: " + ImageSorter.photoSet[tempArray[j]].getShortName() + "<br>");
                    
                }
            }
            else{
                results += ("<br>Folder " + i + ": EMPTY <br>");
            }
        }
        JOptionPane.showMessageDialog(Interface.mainContainer, results);
    }//printImageHierarchy
    
}//SaveImages
