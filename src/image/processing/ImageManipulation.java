/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package image.processing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.Border;

/**
 *
 * @author Catbug
 */
public class ImageManipulation {
    
    //update image label for debug console
    public static void updateDebugImage() throws IOException
    {
            File img = new File(ImageSorter.filePath);
            BufferedImage bimg = ImageIO.read(img);
            ImageIcon image = new ImageIcon(ScaledImage(bimg, 250, 250));
            DebugInterface.imageLabel.setIcon(image);
    }
    
    //update main grid display 
    public static void updateGridImages() throws IOException
    {
        Interface.imagePanel.removeAll();
        
        for(int i=0; i < ImageSorter.photoSet.length;i++)
        {
            Interface.imagePanel.setLayout(new GridLayout(0, 6, 0, 10));
            File img = new File(ImageSorter.photoSet[i].getName());
            BufferedImage bimg = ImageIO.read(img);
            ImageIcon image = new ImageIcon(ScaledImage(bimg, 160, 160));
            Interface.imagePanel.add(new JLabel(image));
            Interface.imagePanel.validate();
            Interface.imagePanel.repaint();
            Interface.imageScrollPane.updateUI();
        }
    }
    
    
    //=====================================//
    // update colour grid
    //=====================================//
    
    public static void initColResultsImages() throws IOException
    {
        Interface.colourResultsPanel.removeAll();
        Interface.colourResultsPanel.setLayout(new GridLayout(0, 4, 0, 0));
    }
    
    public static void updateColResultsImages(int i, int col) throws IOException
    {
        Border border = BorderFactory.createLineBorder(Color.blue);
        
            //TESTING//
            System.out.println("Test: " + ImageSorter.photoSet.length);
        
            File img = new File(ImageSorter.photoSet[i].getName());
            BufferedImage bimg = ImageIO.read(img);
            ImageIcon image = new ImageIcon(ScaledImage(bimg, 80, 80));
            image = new ImageIcon(BorderedImage(image, col));
            Interface.colourResultsPanel.add(new JLabel(image));
    }
    
    public static void finaliseColResultsImages() throws IOException
    {
            Interface.colourResultsPanel.validate();
            Interface.colourResultsPanel.repaint();
            Interface.colourResultsPanel.updateUI();
    }
    
    
    //=====================================//
    // update feature grid
    //=====================================//
    
    public static void initFeatResultsImages() throws IOException
    {
        Interface.featureResultsPanel.removeAll();
        Interface.featureResultsPanel.setLayout(new GridLayout(0, 4, 0, 0));
    }
    
    public static void updateFeatResultsImages(int i, int col) throws IOException
    {
        Border border = BorderFactory.createLineBorder(Color.blue);
        
            File img = new File(ImageSorter.photoSet[i].getName());
            BufferedImage bimg = ImageIO.read(img);
            ImageIcon image = new ImageIcon(ScaledImage(bimg, 80, 80));
            image = new ImageIcon(BorderedImage(image, col));
            Interface.featureResultsPanel.add(new JLabel(image));
    } 
    
    public static void finaliseFeatResultsImages() throws IOException
    {
            Interface.featureResultsPanel.validate();
            Interface.featureResultsPanel.repaint();
            Interface.featureResultsPanel.updateUI();
    }
    
    
    //=====================================//
    // update combined grid
    //=====================================//
    
    public static void initCombResultsImages() throws IOException
    {
        Interface.combinedResultsPanel.removeAll();
        Interface.combinedResultsPanel.setLayout(new GridLayout(0, 4, 0, 0));
    }
    
    public static void updateCombResultsImages(int i, int col) throws IOException
    {
        Border border = BorderFactory.createLineBorder(Color.blue);
        
            File img = new File(ImageSorter.photoSet[i].getName());
            BufferedImage bimg = ImageIO.read(img);
            ImageIcon image = new ImageIcon(ScaledImage(bimg, 80, 80));
            image = new ImageIcon(BorderedImage(image, col));
            Interface.combinedResultsPanel.add(new JLabel(image));
    } 
    
    public static void finaliseCombResultsImages() throws IOException
    {
            Interface.combinedResultsPanel.validate();
            Interface.combinedResultsPanel.repaint();
            Interface.combinedResultsPanel.updateUI();
    }
    
    //rescale image
    static public Image ScaledImage(Image img, int w, int h)
    {
       BufferedImage resizedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImage.createGraphics();
         g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
       g2.drawImage(img, 0, 0, w, h, null);
       g2.dispose();
        return resizedImage;
    }
    
    static public BufferedImage BorderedImage(ImageIcon icon, int col)
    {
        int borderWidth = 1;
        int spaceAroundIcon = 0;
        Color borderColor;
        
        switch (col) {
            case 0:  borderColor = Color.BLUE;
                     break;
            case 1:  borderColor = Color.CYAN;
                     break;
            case 2:  borderColor = Color.GREEN;
                     break;
            case 3:  borderColor = Color.YELLOW;
                     break;
            case 4:  borderColor = Color.MAGENTA;
                     break;
            case 5:  borderColor = Color.ORANGE;
                     break;
            case 6:  borderColor = Color.PINK;
                     break;
            case 7:  borderColor = Color.RED;
                     break;
            case 8:  borderColor = Color.lightGray;
                     break;
            case 9:  borderColor = Color.darkGray;
                     break;
            default: borderColor = Color.BLACK;
                     break;
        }

        BufferedImage bi = new BufferedImage(icon.getIconWidth() + (2 * borderWidth + 2 * spaceAroundIcon),icon.getIconHeight() + (2 * borderWidth + 2 * spaceAroundIcon), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = bi.createGraphics();
        g.setColor(borderColor);
        g.drawImage(icon.getImage(), borderWidth + spaceAroundIcon, borderWidth + spaceAroundIcon, null);

        BasicStroke stroke = new BasicStroke(5); //5 pixels wide (thickness of the border)
        g.setStroke(stroke);

        g.drawRect(0, 0, bi.getWidth() - 1, bi.getHeight() - 1);
        g.dispose();
        
        return bi;
    }
    
}
