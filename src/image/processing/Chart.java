package image.processing;

import java.io.File;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYBarDataset;

public class Chart{
    
    public static void makeColourChart(double[] yvalues, String name)
    {
        
        String filename = getFileName(name);
        double[] xvalues = new double[yvalues.length];
        
        for(int i=0; i < xvalues.length; i++)
        {
            xvalues[i] = i + 1;
        }
        
        double[][] valuepairs = new double[2][];
        valuepairs[0] = xvalues;
        valuepairs[1] = yvalues;
        DefaultXYDataset set = new DefaultXYDataset();
        set.addSeries("Colour bins",valuepairs);      
        XYBarDataset barset = new XYBarDataset(set,0.8);
        JFreeChart chart = ChartFactory.createXYBarChart("Colour Histogram","# of Colour Bins",false,"Quantity", 
                barset,PlotOrientation.VERTICAL,true, true, false);
        JFrame frame = new JFrame(filename + " Histogram testing");
        frame.setContentPane(new ChartPanel(chart));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
   
    }
    
    public static void makeEdgeChart(double[] yvalues, String name)
    {
        String filename = getFileName(name);
        double[] xvalues = new double[yvalues.length];
        
        for(int i=0; i < xvalues.length; i++)
        {
            xvalues[i] = i + 1;
        }
        
        double[][] valuepairs = new double[2][];
        valuepairs[0] = xvalues;
        valuepairs[1] = yvalues;
        DefaultXYDataset set = new DefaultXYDataset();
        set.addSeries("Feature Bins",valuepairs);      
        XYBarDataset barset = new XYBarDataset(set,0.8);
        JFreeChart chart = ChartFactory.createXYBarChart("Feature Histogram","# of Feature Bins",false,"Quantity", 
                barset,PlotOrientation.VERTICAL,true, true, false);
        JFrame frame = new JFrame(filename + " Histogram testing");
        frame.setContentPane(new ChartPanel(chart));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
   
    }
    
    public static String getFileName(String name)
    {
        System.out.println(name);
        String path = name;
        File f = new File(path);
        
        String fName = f.getName();
        return fName.substring(0, fName.lastIndexOf('.'));
    }
    
}

