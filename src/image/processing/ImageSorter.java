//!
package image.processing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
*Image Sorter by Alexander Cooke
*/
public class ImageSorter {
    
    static String filePath = System.getProperty("user.dir") + "/src/image/processing/Stock Images/NEIL2.png";
    static File img;
    static int kColour, kFeature;
    static double kCombined;
    public static Photo[] photoSet;
    public static List<int[]> colClusterOrder;
    public static List<int[]> featClusterOrder;
    public static List<int[]> combClusterOrder;
    
    public static boolean debugState;

    public static void main(String[] args) {
        
        //Check proper main is running
        System.out.println("Default image = " + filePath);
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DebugInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DebugInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DebugInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DebugInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //Open GUI
        Interface gui = new Interface();
        gui.setVisible(true);
        
    }
}
