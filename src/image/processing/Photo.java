
package image.processing;

import java.io.File;

public class Photo {
    
    public String name;
    public int pixelNum;
    public int edgeNum;
    public double[] colourBin;
    public double[] featureBin;
    
    public Photo(String _name, int _pixelNum, int _edgeNum, double[] _colourBin, double[] _featureBin) {
        name = _name;
        pixelNum = _pixelNum;
        edgeNum = _edgeNum;
        colourBin = _colourBin;
        featureBin = _featureBin;
    }
       
    public static void expand() {
        Photo[] newPhotoSet = new Photo[ImageSorter.photoSet.length + 1];
        System.arraycopy(ImageSorter.photoSet, 0, newPhotoSet, 0, ImageSorter.photoSet.length);
        ImageSorter.photoSet = newPhotoSet;
    }
    
    public int getMaxColourBin(){
        int maxColourBin = 0;
        double maxColourBinValue = 0;
        for(int i=0; i<colourBin.length; i++){
            if(colourBin[i] > maxColourBinValue){
                maxColourBin = i;
                maxColourBinValue = colourBin[i];
            }
        }
        return maxColourBin;
    }
    
    public int getMaxFeatureBin(){
        int maxFeatureBin = 0;
        double maxFeatureBinValue = 0;
        for(int i=0; i<featureBin.length; i++){
            if(featureBin[i] > maxFeatureBinValue){
                maxFeatureBin = i;
                maxFeatureBinValue = featureBin[i];
            }
        }
        return maxFeatureBin;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getShortName()
    {
        String tempName = new File(name).getName();
        return tempName;
    }
    
    public int getPixelNum()
    {
        return pixelNum;
    }
    
    public int getEdgeNum()
    {
        return edgeNum;
    }
    
    public double getColourBin(int bin)
    {
        return colourBin[bin];
    }
    
    public double getFeatureBin(int bin)
    {
        return featureBin[bin];
    }
 

    
}
