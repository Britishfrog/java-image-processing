package image.processing;

import java.io.File;
import java.io.FilenameFilter;


public class FolderSearch {
    
    public static int currentImageNum;

    // File representing the folder that you select using a FileChooser
    static File dir = new File(System.getProperty("user.dir") + "/src/image/processing/Stock Images/");

    // array of supported extensions (use a List if you prefer)
    static final String[] EXTENSIONS = new String[]{
        "gif", "png", "bmp", "jpg", "jpeg" // and other formats you need
    };
    // filter to identify images based on their extensions
    static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {

        @Override
        public boolean accept(final File dir, final String name) {
            for (final String ext : EXTENSIONS) {
                if (name.endsWith("." + ext)) {
                    return (true);
                }
            }
            return (false);
        }
    };

    public static void openDirectory() {

        currentImageNum = 0;
        ImageSorter.photoSet = new Photo[0];
        ImageSorter.debugState = false;
        
        if (dir.isDirectory()) { // make sure it's a directory
            
            if(dir.listFiles(IMAGE_FILTER).length > 1) { //make sure number of images is 2 or more
                
                Interface.consoleArea.append("Loading images from directory:\n");
                Interface.consoleArea.append("+++++++++++++++++++++++++++\n");
                
                for (final File f : dir.listFiles(IMAGE_FILTER)) {

                    try {
                    
                        ImageSorter.filePath = f.getAbsolutePath();
                        ImageProcess.processIm(ImageSorter.filePath, currentImageNum, ImageSorter.debugState);
                        currentImageNum++;
                        Interface.consoleArea.append("Loaded: " + f.getName() + "\n");
                    } catch (final Exception e) {
                        // handle errors here
                    }
                    
                }
                
                Interface.consoleArea.append("\n");
                
            }
            
            else{
                Interface.consoleArea.append("You must choose a directory with 2 or more images!\n");
            }
        }
        System.out.println("__________________________________________________________");
        System.out.println("NUMBER OF IMAGES LOADED" + ImageSorter.photoSet.length );
        for( int i = 0; i < dir.listFiles(IMAGE_FILTER).length ; i++)
          System.out.println( dir.listFiles(IMAGE_FILTER)[i] );
        System.out.println("__________________________________________________________");
        
        //Printout details for every photo in set
        /*for(int i=0; i < ImageSorter.photoSet.length;i++)
        {
            
            Interface.testArea.append("\n");
            Interface.testArea.append("File name: \"" + ImageSorter.photoSet[i].getName() + "\"\n");
            Interface.testArea.append("Number of pixels: \"" + ImageSorter.photoSet[i].getPixelNum() + "\"\n");
            Interface.testArea.append("Number of edges: \"" + ImageSorter.photoSet[i].getEdgeNum() + "\"\n");
            Interface.testArea.append("Max Colour Bin: \"" + (ImageSorter.photoSet[i].getMaxColourBin() + 1) + "\"\n");
            Interface.testArea.append("Max Feature Bin: \"" + (ImageSorter.photoSet[i].getMaxFeatureBin() + 1) + "\"\n");
            Interface.testArea.append("Total of max colour bin: \"" 
                    + ImageSorter.photoSet[i].getColourBin(ImageSorter.photoSet[i].getMaxColourBin()) + "\"\n");
            Interface.testArea.append("Total of max feature bin: \"" 
                    + ImageSorter.photoSet[i].getFeatureBin(ImageSorter.photoSet[i].getMaxFeatureBin()) + "\"\n");
            
        }*/
    
    }
    
}